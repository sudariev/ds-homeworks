// 1) Опишіть своїми словами як працює метод forEach.

// Метод слугує для перебору масиву. Для кожного з його елементів він почергово
// викликає функцію і передає в неї 3 аргументи - поточний елемент масива, його номер та власне сам масив.

// 2) Як очистити масив?

// Якщо масиву задати довжину 0, то він очиститься.
// arr.length = 0;

// 3) Як можна перевірити, що та чи інша змінна є масивом?

// За допомогою оператора typeof - у масива має повернутись 'object'.
// Щоправда, у null та object - також; але у масива можна запросити вивід елемента по індексу,
// навідміну від null та object.

function filterBy(array, check) {
    let newArray = [];

    for (let item = 0; item < array.length; item++) {
        if (typeof array[item] !== check) {
            newArray.push(array[item]);
        }
    }
    return newArray;
}
console.log(filterBy(["hello", "world", 23, "23", null], null));

// 2-й варіант

function filterBy(array, check) {
    newArray = array.filter(function (item) {
        return typeof item !== check;
    });
    return newArray;
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));
