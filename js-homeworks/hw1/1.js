// var - устаревший формат объявления переменных. var-переменная доступна (видна) в области функции.
// let - используется для объяления изменяемых переменных и видна только в области блока и только после объявления.
// const - все то же что и для let, но переменная не может быть изменена.
// Хотя само значение переменной const не меняется, содержимое в ней может быть измененено, например, в массиве.

const name = prompt('Enter your name', 'name');
const age = +prompt('How old are you?', 17);

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age < 23) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome, ${name}`);
    } else {
        alert('You aren\'t allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}`);
}
