// 1. Функции в программировании нужны для выполения задач - мат. вычислений, преобразований, действий с переменными и объектами.
// 2. Аргументы в функцию передаются для работы функции с разными данными (значениями, переменными),
// которые могут неоднократно меняться. Таким образом функции можно использовать повторно с разными значениями и целями.
const firstNumber = +prompt('Enter first number');
const secondNumber = +prompt('Enter second number');
const operation = prompt('Enter desired operation: +, -, *, /');

const firstNum = firstNumber;
const secondNum = secondNumber;
const oper = operation;

function calc (firstNum, secondNum, oper) {
    switch (oper) {
        case '+':
            return ('summ = ' + (firstNum + secondNum));
        case '-': 
            return ('substraction = ' + (firstNum - secondNum));
        case '*':
            return ('multiplication = ' + (firstNum * secondNum));
        case '/':
            return ('division = ' + (firstNum / secondNum));
        default:
            alert('You haven\'t enter operation.');
    }
}
console.log(calc(firstNum, secondNum, oper));