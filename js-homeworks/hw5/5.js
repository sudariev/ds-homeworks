//  Экранирование - применение специальных символов в тексте и преобразование их в обычный текст, в частности, если они могут быть восприняты как функциональные символы.

const createNewUser = () => {
    const newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your surname"),
        birthday: prompt("Enter your birth date in format dd.mm.yyyy"),
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const birthdate = Date.parse(
                this.birthday.slice(6) -
                    (this.birthday.slice(3, 4) - 1) -
                    this.birthday.slice(0, 1)
            );
            const date = Date.parse(new Date());
            const age = parseInt(
                (date - birthdate) / (365.25 * 24 * 3600 * 1000)
            );
            return age;
        },
        getPassword() {
            return (
                this.firstName[0].toUpperCase() +
                this.lastName.toLowerCase() +
                this.birthday.slice(6)
            );
        },
    };
    return newUser;
};
const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
